require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import { Link } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

class AppComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  handleToggle = () => this.setState({open: !this.state.open});

  handleClose = () => this.setState({open: false});

  render() {
    return (
      <MuiThemeProvider>
        <div>
          <AppBar title="React Webpack"
                  onLeftIconButtonTouchTap={this.handleToggle}
                  className="main-toolbar"/>
          <Drawer docked={false}
                  open={this.state.open}
                  onRequestChange={(open) => this.setState({open})}>
            <MenuItem onTouchTap={this.handleClose}><Link className="sidebar-menu-item" to="/">Dashboard</Link></MenuItem>
            <MenuItem onTouchTap={this.handleClose}><Link className="sidebar-menu-item" to="/users">Users</Link></MenuItem>
          </Drawer>
          <div className="content">
            {this.props.children}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default AppComponent;
