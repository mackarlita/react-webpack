import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import Login from './components/Login';
import App from './components/App'
import Dashboard from './components/dashboard/Dashboard';
import UsersList from './components/users/Users';

// Needed for onTouchTap
injectTapEventPlugin();

// Routes declaration
const Routes = (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Dashboard}></IndexRoute>
      <Route path="users" component={UsersList}></Route>
    </Route>
    <Route path="/login" component={Login}></Route>
  </Router>
);

// Render the main component into the dom
ReactDOM.render(Routes, document.getElementById('app'));
